var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;

/* Initialize a default map centered on bordeaux */

function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer(); //Take the direction
    var Bordeaux = new google.maps.LatLng(44.8404400, -0.5805000);
    var mapOptions = { //default values
        zoom:7,
        center: Bordeaux
    }
    map = new google.maps.Map(document.getElementById("map"), mapOptions); //Display the map
    directionsDisplay.setMap(map);
}

/* Calculate the route */

function calcRoute() {

    var start = $(document.getElementById("mapInput")).val(); //get the value of the input -> departure
    var end = "15 rue naudet 33175 Gradignan"; //arrival
    var request = { //use the value entered before
        origin:start,
        destination:end,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function(result, status) { //ask google to give the correct route
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result); //Display the result
        }
    });
}
