/* GLOBAL WIDGET BOX STATUS */
var isFiredDateTime = false;
var isFiredWeather = false;
var isFiredPics = false;
var isFiredTwitter = false;
var isFiredYoutube = false;
var isFiredMaps = false;
var isFiredSports = false;

/* YOUTUBE RUNNING CHECKER */
var isYoutubePlaying = false;


/* WIDGET BOX PANE SIZING AND VIEWS*/
function resizeOnLoad(){
    var sidebarWidth = $("ul#nav-mobile").width();
    var windowWith = $(window).width();
    document.getElementById("dockingSystem").style.marginLeft = sidebarWidth + "px";
    document.getElementById("dockingSystem").style.width = windowWith-sidebarWidth + "px";

    for(var i = 1; i < 8; i++){
        var widgetBoxSelector = "widgetBox" + i;
        var selectedWidgetBox = document.getElementById(widgetBoxSelector);

        $( selectedWidgetBox ).fadeOut( "slow", function() {

        });
    }
}

/* ADDITION AND DELETION */

//Get first available widgetBox
function getFirstWidgetPlace(){
    var emptyBlockList = $('table td:empty');
    if(emptyBlockList.size() >= 1){
        return emptyBlockList.first().attr("id");
    }
    else{
        return null;
    }
}

//Empty all widgetBoxes
function clearAllWidgets(){

    for(var i = 1; i < 8; i++){
        var widgetBoxSelector = "widgetBox" + i;
        var selectedWidgetBox = document.getElementById(widgetBoxSelector);

        $( selectedWidgetBox ).fadeOut( "slow", function() {

        });

        $(selectedWidgetBox).empty();
    }

    isFiredDateTime = false;
    isFiredWeather = false;
    isFiredPics = false;
    isFiredTwitter = false;
    isFiredYoutube = false;
    isFiredMaps = false;
    isFiredSports = false;

}

//Empty a widgetBox
function emptyBox(widgetBoxToEmpty){
    var selectedWidgetBoxId = $(widgetBoxToEmpty).attr("id");
    var selectedWidgetBox = document.getElementById(selectedWidgetBoxId);

    $( selectedWidgetBox ).fadeOut( "slow", function() {
        var widgetBoxTypeIdentificator = $(selectedWidgetBox).children(0).attr("id");
        resetWidgetBoxAvailability(getWidgetBoxContent(widgetBoxTypeIdentificator));
        $(selectedWidgetBox).empty();
    });
}

/* MANAGE CONTENT */

//Get content of a widgetBox
function getWidgetBoxContent(widgetBoxId){
    switch (widgetBoxId){
        case "DateTimeBox":
            return "DateTime";
            break;
        case "WeatherBox":
            return "Weather";
            break;
        case "PicsBox":
            return "Pics";
            break;
        case "TwitterBox":
            return "Twitter";
            break;
        case "YoutubeBox":
            return "Youtube";
            break;
        case "MapsBox":
            return "Maps";
            break;
        case "SportsBox":
            return "Sports";
            break;
        default:
            break;
    }
}

/* Reset availability of widgetBox */
function resetWidgetBoxAvailability(widgetBoxContent){
    switch(widgetBoxContent){
        case "DateTime":
            isFiredDateTime = false;
            break;
        case "Weather":
            isFiredWeather = false;
            break;
        case "Pics":
            isFiredPics = false;
            break;
        case "Twitter":
            isFiredTwitter = false;
            break;
        case "Youtube":
            isYoutubePlaying = false;
            isFiredYoutube = false;
            break;
        case "Maps":
            isFiredMaps = false;
            break;
        case "Sports":
            isFiredSports = false;
            break;
        default:
            break;
    }
}

/* REACTION TO MENU LINKS */

//Pop a new date and time widget
function fireNewDateTime(){
    var selectedWidgetBox = getFirstWidgetPlace();
    if(selectedWidgetBox != null && isFiredDateTime == false){

        var millisDate = new Date;
        millisDate.setTime(millisDate.getTime());

        var currentDate = millisDate.getDate() + "/" + getCorrectDateFormat(millisDate.getMonth()) + "/" + millisDate.getFullYear();
        var currentTime = getCorrectTimeFormat(millisDate.getHours()) + ":" + getCorrectTimeFormat(millisDate.getMinutes()) + ":" + getCorrectTimeFormat(millisDate.getSeconds());

        var relatedWidgetBox = document.getElementById(selectedWidgetBox);
        var innerBox = document.createElement("div");
        innerBox.setAttribute("id","DateTimeBox");

        $(relatedWidgetBox).append(innerBox);
        $(innerBox).append("<h1 class='DateTimeTitle'>Date</h1>");
        $(innerBox).append("<h3 id='dateData'>" + currentDate + "</h3>");
        $(innerBox).append("<h1 class='DateTimeTitle'>Heure</h1>");
        $(innerBox).append("<h3 id='timeData'>" + currentTime + "</h3>");
        $(innerBox).append("<div class='button-center'><a id=\"dateTimeDelete\" class=\"waves-effect waves-light btn\" onclick='emptyBox(" + selectedWidgetBox + ")'><i class=\"material-icons left right\">delete</i></a></div>");

        $( relatedWidgetBox ).fadeIn( "slow", function() {

        });

        isFiredDateTime = true;
    }
}

function fireNewWeather(){
    var selectedWidgetBox = getFirstWidgetPlace();
    if(selectedWidgetBox != null && isFiredWeather == false){
        var relatedWidgetBox = document.getElementById(selectedWidgetBox);
        var innerBox = document.createElement("div");
        innerBox.setAttribute("id","WeatherBox");
        $(relatedWidgetBox).append(innerBox);

        var displayWeather = document.createElement("div");
        displayWeather.setAttribute("id","displayWeather");

        //Add Form

        var town = document.createElement("div");
        town.setAttribute("id", "weather");
        town.setAttribute("class", "input-field col s6");

        var searchIcon = document.createElement("i");
        searchIcon.setAttribute("class", "material-icons prefix");
        $(searchIcon).text("search");

        var searchInput = document.createElement("input");
        searchInput.setAttribute("id","weatherInput");
        searchInput.setAttribute("type","text");
        searchInput.setAttribute("class","validate");

        var searchLabel = document.createElement("label");
        searchLabel.setAttribute("for", "icon-prefix");
        $(searchLabel).text("Ville");


        town.appendChild(searchIcon);
        town.appendChild(searchInput);
        town.appendChild(searchLabel);
        $(town).append("<div class='button-center'><a id=\"departureEntry\" class=\"waves-effect waves-light btn\" onclick='getWeather()' >Afficher la météo</a></div>");

        $(innerBox).append(town);
        $(innerBox).append(displayWeather);

        $(innerBox).append("<div class='button-center' style='margin-bottom: 10px;'><a id=\"weatherDelete\" class=\"waves-effect waves-light btn\" onclick='emptyBox(" + selectedWidgetBox + ")'><i class=\"material-icons left right\">delete</i></a></div>");
        $( relatedWidgetBox ).fadeIn( "slow", function() {

        });

        isFiredWeather = true;
    }
}

function fireNewPics(){
    var selectedWidgetBox = getFirstWidgetPlace();
    if(selectedWidgetBox != null && isFiredPics == false){

        var relatedWidgetBox = document.getElementById(selectedWidgetBox);
        var innerBox = document.createElement("div");
        innerBox.setAttribute("id","PicsBox");
        $(relatedWidgetBox).append(innerBox);

        var displayPics = document.createElement("div");
        displayPics.setAttribute("id","displayPics");

        //Add Form

        var picName = document.createElement("div");
        picName.setAttribute("id", "flickr");
        picName.setAttribute("class", "input-field col s6");

        var searchIcon = document.createElement("i");
        searchIcon.setAttribute("class", "material-icons prefix");
        $(searchIcon).text("search");

        var searchInput = document.createElement("input");
        searchInput.setAttribute("id","flickrInput");
        searchInput.setAttribute("type","text");
        searchInput.setAttribute("class","validate");

        var searchLabel = document.createElement("label");
        searchLabel.setAttribute("for", "icon-prefix");
        $(searchLabel).text("Mot-clé");


        picName.appendChild(searchIcon);
        picName.appendChild(searchInput);
        picName.appendChild(searchLabel);
        $(picName).append("<div class='button-center'><a id=\"departureEntry\" class=\"waves-effect waves-light btn\" onclick='getPics(" + selectedWidgetBox + ")' >Rechercher une photo</a></div>");

        $(innerBox).append(picName);
        $(innerBox).append(displayPics);

        $(innerBox).append("<div class='button-center' style='margin-bottom: 10px;'><a id=\"weatherDelete\" class=\"waves-effect waves-light btn\" onclick='emptyBox(" + selectedWidgetBox + ")'><i class=\"material-icons left right\">delete</i></a></div>");
        $( relatedWidgetBox ).fadeIn( "slow", function() {

        });

        isFiredPics = true;
    }
}

function fireNewTwitter(){
    var selectedWidgetBox = getFirstWidgetPlace();
    if(selectedWidgetBox != null && isFiredTwitter == false){
        var widgetBoxContent = "";

        var relatedWidgetBox = document.getElementById(selectedWidgetBox);

        var innerBox = document.createElement("div");
        var twitterWidgetBox = document.createElement("div");

        innerBox.setAttribute("id","TwitterBox");
        $(relatedWidgetBox).append(innerBox);
        $(innerBox).append(twitterWidgetBox);
        $(twitterWidgetBox).load("modules/TwitterModule.html");
        $(innerBox).append(widgetBoxContent);
        $(innerBox).append("<div class='button-center'><a id=\"twitterDelete\" class=\"waves-effect waves-light btn\" onclick='emptyBox(" + selectedWidgetBox + ")'><i class=\"material-icons left right\">delete</i></a></div>");

        $( relatedWidgetBox ).fadeIn( "slow", function() {

        });

        isFiredTwitter = true;
    }
}

function fireNewYoutube(){
    var selectedWidgetBox = getFirstWidgetPlace();
    if(selectedWidgetBox != null && isFiredYoutube == false){
        var relatedWidgetBox = document.getElementById(selectedWidgetBox);
        var innerBox = document.createElement("div");
        innerBox.setAttribute("id","YoutubeBox");
        $(relatedWidgetBox).append(innerBox);

        //Add searchbar

        var searchBlock = document.createElement("div");
        searchBlock.setAttribute("id", "youtubeSearchBox");
        searchBlock.setAttribute("class", "input-field col s6");

        var searchIcon = document.createElement("i");
        searchIcon.setAttribute("class", "material-icons prefix");
        $(searchIcon).text("search");

        var searchInput = document.createElement("input");
        searchInput.setAttribute("id","icon-prefix");
        searchInput.setAttribute("type","text");
        searchInput.setAttribute("class","validate");

        var searchLabel = document.createElement("label");
        searchLabel.setAttribute("for", "icon-prefix");
        $(searchLabel).text("Rechercher...")



        searchBlock.appendChild(searchIcon);
        searchBlock.appendChild(searchInput);
        searchBlock.appendChild(searchLabel);

        $(innerBox).append(searchBlock);

        $(searchBlock).append("<div class='button-center'><a id=\"youtubeSearch\" class=\"waves-effect waves-light btn\" onclick='PopYoutubePlayer(" + selectedWidgetBox + ")' >Rechercher</a></div>");
        $(searchBlock).append("<div class='button-center' style='margin-bottom: 10px;'><a id=\"youtubeDelete\" class=\"waves-effect waves-light btn\" onclick='emptyBox(" + selectedWidgetBox + ")'><i class=\"material-icons left right\">delete</i></a></div>");


        $( relatedWidgetBox ).fadeIn( "slow", function() {

        });
        isFiredYoutube = true;
    }
}


function fireNewMaps(){
    var selectedWidgetBox = getFirstWidgetPlace();
    if(selectedWidgetBox != null && isFiredMaps == false){
        var relatedWidgetBox = document.getElementById(selectedWidgetBox);
        var innerBox = document.createElement("div");
        var map = document.createElement("div");
        map.setAttribute("id","map");
        $(map).css("width",$(relatedWidgetBox).width() - 90);
        $(map).css("height",300);
        innerBox.setAttribute("id","MapsBox");
        $(selectedWidgetBox).css("width",420);
        $(relatedWidgetBox).append(innerBox);

        //Add Form

        var Départ = document.createElement("div");
        Départ.setAttribute("id", "mapDeparture");
        Départ.setAttribute("class", "input-field col s6");

        var searchIcon = document.createElement("i");
        searchIcon.setAttribute("class", "material-icons prefix");
        $(searchIcon).text("search");

        var searchInput = document.createElement("input");
        searchInput.setAttribute("id","mapInput");
        searchInput.setAttribute("type","text");
        searchInput.setAttribute("class","validate");

        var searchLabel = document.createElement("label");
        searchLabel.setAttribute("for", "icon-prefix");
        $(searchLabel).text("Départ");



        Départ.appendChild(searchIcon);
        Départ.appendChild(searchInput);
        Départ.appendChild(searchLabel);
        $(Départ).append("<div class='button-center'><a id=\"departureEntry\" class=\"waves-effect waves-light btn\" onclick='calcRoute()' >Itinéraire</a></div>");

        $(innerBox).append(Départ);
        $(innerBox).append(map);
        initialize();
        $(innerBox).append("<div class='button-center'><a id=\"mapDelete\" class=\"waves-effect waves-light btn\" onclick='emptyBox(" + selectedWidgetBox + ")'><i class=\"material-icons left right\">delete</i></a></div>");
        $( relatedWidgetBox ).fadeIn( "slow", function() {

        });
        isFiredMaps = true;
    }
}

function fireNewSports(){
    var selectedWidgetBox = getFirstWidgetPlace();
    if(selectedWidgetBox != null && isFiredSports == false){
        var relatedWidgetBox = document.getElementById(selectedWidgetBox);
        var innerBox = document.createElement("div");
        innerBox.setAttribute("id","SportsBox");

        var displayName =  document.createElement("div");
        displayName.setAttribute("id","info");

        var displayPlayer =  document.createElement("div");
        displayPlayer.setAttribute("id","player");

        var tabPlayerHead =  document.createElement("table");
        tabPlayerHead.setAttribute("id","tabPlayer");
        tabPlayerHead.setAttribute("class","responsive-table");
        tabPlayerHead.setAttribute("class","centered");
        tabPlayerHead.innerHTML = "<thead><tr><th>Nom du joueur</th><th>Position du joueur</th></tr>";

        var tabPlayer = document.createElement("tbody");
        tabPlayer.setAttribute("id","tbody");

        tabPlayerHead.appendChild(tabPlayer);
        displayPlayer.appendChild(tabPlayerHead);

        $(relatedWidgetBox).append(innerBox);
        $(innerBox).append(displayName);
        $(innerBox).append(displayPlayer);

        getTeam();
        $(innerBox).append("<div class='button-center'><a id=\"sportDelete\" class=\"waves-effect waves-light btn\" onclick='emptyBox(" + selectedWidgetBox + ")'><i class=\"material-icons left right\">delete</i></a></div>");

        $( relatedWidgetBox ).fadeIn( "slow", function() {

        });
        isFiredSports = true;
    }
}

/* DATE AND TIME WIDGET HANDLERS */

//Date and time renewers
setInterval(function(){
   if(isFiredDateTime){
       var dateInWidgetBox = document.getElementById("dateData");
       var timeInWidgetBox = document.getElementById("timeData");

       var millisDate = new Date;
       millisDate.setTime(millisDate.getTime());

       var currentDate = millisDate.getDate() + "/" + getCorrectDateFormat(millisDate.getMonth()) + "/" + millisDate.getFullYear();
       var currentTime = getCorrectTimeFormat(millisDate.getHours()) + ":" + getCorrectTimeFormat(millisDate.getMinutes()) + ":" + getCorrectTimeFormat(millisDate.getSeconds());

       $(dateInWidgetBox).html(currentDate);
       $(timeInWidgetBox).html(currentTime);
   }
}, 1000);


//Handle month 1-late and 0X format
function getCorrectDateFormat(data){
    if(data.toString().length == 1){
        var sizedValue = data+1;
        if(sizedValue == 10){
            return sizedValue;
        }
        else{
            return "0" + sizedValue;
        }

    }
    else
        return data+1;
}

//Handle HMS 0X format
function getCorrectTimeFormat(timeData){
    if(timeData.toString().length == 1)
        return "0" + timeData;
    else
        return timeData;
}

/* YOUTUBE WIDGET HANDLER */

function PopYoutubePlayer(widgetBoxId){
    var searchWord = $(document.getElementById("icon-prefix")).val();
    var widgetWidth = $(widgetBoxId).width() - 20;
    var widgetHeight = widgetWidth * (2/3);
    var widgetBoxContent = "<iframe id=\"ytplayer\" type=\"text/html\" width=\"" + widgetWidth + "\" height=\"" + widgetHeight + "\" src=\"http://www.youtube.com/embed?listType=search&list=" + searchWord + "\" frameborder=\"0\" />";

    if(isYoutubePlaying == true){
        widgetBoxContent = "<iframe id=\"ytplayer\" type=\"text/html\" width=\"" + widgetWidth + "\" height=\"" + widgetHeight + "\" src=\"http://www.youtube.com/embed?listType=search&list=" + searchWord + "\" frameborder=\"0\" />";
        $("#ytplayer").remove();
        isYoutubePlaying = false;
    }

    var playerPlaceholder = document.getElementById("YoutubeBox");

    $(playerPlaceholder).append(widgetBoxContent);

    isYoutubePlaying = true;
}
/* WEATHER WIDGET HANDLER */

function getWeather(){
    var town = $(document.getElementById("weatherInput")).val();
    var url = "http://api.openweathermap.org/data/2.5/weather?q="+ town +"&appid=8754001be4624878ec1c248f4d18e261"
    var obj = Get(url);
    var jsonWeather = JSON.parse(obj);

    var sunriseEpoch = jsonWeather.sys.sunrise;
    var sunsetEpoch = jsonWeather.sys.sunset;

    var formattedSunrise = getHourFromEpoch(sunriseEpoch);
    var formattedSunset = getHourFromEpoch(sunsetEpoch);

    var weatherDisplayer = document.getElementById("displayWeather");

    $(weatherDisplayer).empty();
    $(weatherDisplayer).append("<h2 id=\"weatherCity\">" + jsonWeather.name + "</h2>");

    var dataList = document.createElement("ul");
    dataList.setAttribute("id","weatherDataList");
    $(dataList).append("<li>Météo actuelle : <img id=\"WeatherIcon\" src=\"http://openweathermap.org/img/w/" + jsonWeather.weather[0].icon + ".png\"> (" + jsonWeather.weather[0].description + ") </li>");
    $(dataList).append("<li>Température actuelle : " + Math.trunc(jsonWeather.main.temp - 273.15) + "°C </li>");
    $(dataList).append("<li>Vitesse du vent : " + Math.trunc(jsonWeather.wind.speed*3.6) + " km/h</li>");
    $(dataList).append("<li>Humidité : " + Math.trunc(jsonWeather.main.humidity) + "% </li>");
    $(dataList).append("<li>Lever du soleil : " + formattedSunrise + "</li>");
    $(dataList).append("<li>Coucher du soleil : " + formattedSunset + "</li>");

    $(weatherDisplayer).append(dataList);
}


function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;

}

function getHourFromEpoch(epochTime){
    var hours = 0;
    var minutes = 0;
    var dayJump = "";

    //Suppress days
    while(epochTime > 86400){
        epochTime -= 86400;
    }

    //Delete hours
    while(epochTime > 3600){
        epochTime-= 3600
        hours++;
    }

    //Delete minutes
    while(epochTime > 60){
        epochTime-=60;
        minutes++;
    }

    //Handle UTC
    var offset = ((new Date().getTimezoneOffset())/60)*(-1);
    hours += offset;

    //Handle dayjump
    if(hours >= 24){
        hours -= 24;
        dayJump = " (J+1) ";
    }

    //HMS 0X Formatting
    if(hours < 10){
        hours = "0" + hours;
    }

    if(minutes < 10){
        minutes = "0" + minutes;
    }

    //Add UTC range
    return hours + ":" + minutes + " (UTC +" + offset + ") " + dayJump;
}

/*SPORT WIDGET HANDLER */

function getTeam(){
    $.ajax({
        headers: { 'X-Auth-Token': 'e81bd6daa983423aa8cc022a1cc0ad36' },
        url: 'http://api.football-data.org/v1/teams/66',
        dataType: 'json',
        type: 'GET'
    }).done(function(response) {
        document.getElementById("info").innerHTML = "<h2 id=\"footballTeam\">" + response.name + "</h2></br>";
        getPlayers();

    });
}

function getPlayers(){
    $.ajax({
        headers: { 'X-Auth-Token': 'e81bd6daa983423aa8cc022a1cc0ad36' },
        url: "http://api.football-data.org/v1/teams/66/players",
        dataType: 'json',
        type: 'GET'
    }).done(function(response) {
        for (var i = 0;i<response.count;i++){
            document.getElementById("tbody").innerHTML += "<tr><td class=\"playerStatus\">" +response.players[i].name + "</td><td class=\"playerStatus\"> "+ response.players[i].position +"</td></tr>" ;
        }
    });
}

/* PHOTO WIDGET HANDLER */

function getPics(widgetBoxId){
    var picturePlaceholder = document.getElementById("PicsBox");
    var oldPicturePlaces = document.getElementById("picturePlace");

    if(oldPicturePlaces != null){
        $(oldPicturePlaces).remove();
    }

    var picturePlaces = document.createElement("div");
    picturePlaces.setAttribute("id","picturePlace");

    var picWord = $(document.getElementById("flickrInput")).val();
    var privateKey = "a475d8379efc41f69d62a7fc9503095c";
    var per_page = "10";
    var format = "json";
    var flickurl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+ privateKey +
        "&tags="+picWord+"&per_page="+ per_page +"&format="+format+"&nojsoncallback=1";
    var stagedPic = false;

    $.getJSON(flickurl,function(json){
        $.each(json.photos.photo,function(i,myresult){
            apiurl_size = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=" + privateKey + "&photo_id="+myresult.id+"&format=json&nojsoncallback=1";
            $.getJSON(apiurl_size,function(size){
                $.each(size.sizes.size,function(i,myresult_size){
                    var selected_size = 240;
                    if(myresult_size.width==selected_size && stagedPic == false){
                            stagedPic = true;
                            $(picturePlaces).append('<img src="'+myresult_size.source+'" width="' + ($(widgetBoxId).width() - 20 ) + '"/>');
                    }
                })
            })
        });
    });
    $(picturePlaceholder).append(picturePlaces);
}